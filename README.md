# Post-sync

WordPress plugin publishes new posts once a day.
Also it show recent posts via short code [ps_recent_posts]
ps_recent_posts can apply attributes:
- title - shows h2 before posts (default: Recent posts);
- count - number posts to show (default: 5);
- sort - can be date, title or rating (default: title);
- ids  - posts id to show;