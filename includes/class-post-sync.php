<?php

require_once( ABSPATH . '/wp-admin/includes/taxonomy.php');
require_once(ABSPATH . '/wp-admin/includes/image.php');

class PostSync {

    private $key;
    private $url;

    function __construct(){
        $this->key = '413dfbf0';
        $this->url = 'http://my.api.mockaroo.com/posts.json';
    }

    public function getPosts(): array {

        $curl = curl_init($this->url);
        curl_setopt($curl, CURLOPT_URL, $this->url); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $headers = array(
           "X-API-Key: ".$this->key,
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        
        $response = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($response,true);
        var_dump($json);
        return $json; 
    }

    public function cronStart(): void {
		
        $posts = $this->getPosts();
        foreach($posts as $post):
            if( !empty( get_posts([ 'title' => $post['title'], 'post_type' => 'post' ]) ) ) continue;
            $this->addPost($post);
        endforeach;
		
    }

    public function addPost(array $post): bool {

        if(empty($post['title']) || empty($post['content'])) return false;
        $author = get_users(array('role' => 'administrator', 'number' => 1))[0]->ID;
        $post_data = array(
            'post_title'   => $post['title'],
            'post_content' => $post['content'],
            'post_status'  => 'publish',
            'post_author'  => $author,
            'post_date'    => $this->randomDate(),
            'post_type'    => 'post'
        );
        $post_id = wp_insert_post($post_data);

        if (!is_wp_error($post_id)):
            if(!empty($post['site_link'])) add_post_meta($post_id, 'ps_site_link', $post['site_link']);
            if(!empty($post['rating'])) add_post_meta($post_id, 'ps_rating', $post['rating']);
            if(!empty($post['category'])) $this->postSetCategory($post_id, $post['category']);
            if(!empty($post['image'])) $this->postSetThumbnail($post_id, $post['image']);
            return true;
        else:
            return false;
        endif;

    }

    public function randomDate(): string {
        $min_date = strtotime('-1 month');
        $max_date = time();
        $random_date = mt_rand($min_date, $max_date);
        $date = date('Y-m-d H:i:s', $random_date);
        return $date;        
    }

    public function postSetCategory(int $post_id, string $category_name): bool {

        $category = get_term_by('name', $category_name, 'category');
        if (!$category) {
            $args = array(
                'cat_name' => $category_name,
                'taxonomy' => 'category',
            );
            $category_id = wp_insert_category($args);
            if (!is_wp_error($category_id)) {
                $category = get_term($category_id, 'category');
            }else{
                return false;
            }
        }
    
        if ($category) {
            wp_set_post_categories($post_id, array($category->term_id));
            return true;
        }else{
            return false;
        }

    }

    public function postSetThumbnail(int $post_id, string $image_url): void {
        
        $upload_dir = wp_upload_dir();
        $image_data = file_get_contents($image_url);

        preg_match('/\/([^\/]+\.(png|jpeg|jpg))/', $image_url, $matches);
        if (isset($matches[1])) {
            $filename = $matches[1];
        } else {
            $filename = basename($image_url);
        }
        if(wp_mkdir_p($upload_dir['path']))
          $file = $upload_dir['path'] . '/' . $filename;
        else
          $file = $upload_dir['basedir'] . '/' . $filename;
        file_put_contents($file, $image_data);

        $wp_filetype = wp_check_filetype($filename, null );
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        wp_update_attachment_metadata( $attach_id, $attach_data );
        set_post_thumbnail( $post_id, $attach_id );
    }

}