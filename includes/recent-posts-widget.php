<?php

function ps_recent_posts_function($atts) {
    
    wp_enqueue_style('post_sync_style');

    $param = shortcode_atts( array(
        'title' => __('Recent posts','post-sync'),
        'count' => '5',
        'sort' => 'title',
        'ids' => null,
    ), $atts );

    $args = array(
        'post_type'      => 'post',
        'posts_per_page' => $param['count'],
    );

    if(!empty($param['ids'])){
        $ids = explode(',', $param['ids']);
        $args['post__in'] = $ids;
    }

    switch ( $param['sort'] ) {
        case 'title':
            $args['orderby'] = 'title';
            $args['order']   = 'ASC';
            break;
        case 'date':
            $args['orderby'] = 'date';
            $args['order']   = 'DESC';
            break;
        case 'rating':
            $args['orderby'] = 'meta_value_num';
            $args['meta_key'] = 'ps_rating';
            $args['order']    = 'DESC';
            break;
        default:
            $args['orderby'] = 'title';
            $args['order']   = 'ASC';
            break;
    }


    
    $posts = get_posts( $args );

    ob_start();

    echo '<div class="ps__recent_posts">';
    echo "<h2>".$param['title']."</h2>";

    foreach ( $posts as $post ) {
        include( POST_SYNC_PATH . 'templates/recent-post.php' );
    }
    echo '</div>';
    $html = ob_get_clean();

    return $html;
}
add_shortcode('ps_recent_posts', 'ps_recent_posts_function');