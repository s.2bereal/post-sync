<?php
/**
 * Plugin Name:       Posts sync
 * Plugin URI:        http://localhost/
 * Description:       Wordpress plugin for automatic adding new posts from API
 * Version:           1.0
 * Author:            Serg K
 * Text Domain:       post-sync
 */

define( 'POST_SYNC_PLUGIN_NAME', 'post-sync' );
define( 'POST_SYNC_VERSION', '1.0.0' );
define( 'POST_SYNC_PATH', plugin_dir_path( __FILE__ ) );

require_once POST_SYNC_PATH . '/includes/class-post-sync.php';
require_once POST_SYNC_PATH . '/includes/recent-posts-widget.php';

function activate_post_sync() {
    if ( ! wp_next_scheduled( 'post_sync_cron' ) ) {
        wp_schedule_event( time(), 'daily', 'post_sync_cron' );
    }
}

function deactivate_post_sync() {
    wp_clear_scheduled_hook( 'post_sync_cron_hook' );
}

register_activation_hook( __FILE__, 'activate_post_sync' );
register_deactivation_hook( __FILE__, 'deactivate_post_sync' );

function post_sync_cron_hook(){
    $PostSync = new PostSync();
    $PostSync->cronStart();
}
add_action( 'post_sync_cron', 'post_sync_cron_hook' );

function post_sync_styles() {
    if (has_shortcode(get_the_content(), 'ps_recent_posts')) {
        wp_register_style('post_sync_styles', plugins_url('/assets/css/style.css', __FILE__) );
        wp_enqueue_style('post_sync_styles');
    }
}
add_action('wp_enqueue_scripts', 'post_sync_styles');