<?php
$category_name = get_the_category_by_ID( $post->ID );
$rating = 1.2;//get_post_meta( $post->ID, 'ps_rating', true );
$site_link = 1;//get_post_meta( $post->ID, 'ps_site_link', true );
$categories = get_the_category($post->ID);
$image = get_the_post_thumbnail_url($post->ID);
?>

<div class="ps__recent_post">
    <?php if($image):?>
    <div class="ps__recent_post-image">
        <img src="<?php echo $image; ?>" alt="<?php echo $post->post_title ?>">
    </div>
    <?php endif;?>
    <div class="ps__recent_post-content">
        <?php if ($categories):?>
        <div class="ps__recent_post-categories">
            <?php foreach ($categories as $category) {?>
            <div><? echo $category->name; ?></div>
            <?php } ?>
        </div>
        <?php endif;?>
        <div class="ps__recent_post-title"><?php echo $post->post_title ?></div>
        <div class="ps__recent_post-box">
            <div class="ps__recent_post-readmore">
                <a href="<?php the_permalink($post->ID)?>"  ><?php _e('Read more', 'post-sync')?></a>
            </div>
            <div class="ps__recent_post-meta">
                <?php if(!empty($rating)): ?>
                <div class="ps__recent_post-rating">
                    &#11088; <?php echo $rating;?>
                </div>
                <?php endif;?>
                
                <?php if(!empty($site_link)): ?>
                <div class="ps__recent_post-link">
                    <a href="<?php echo $site_link; ?>" rel="nofollow" target="_blank" ><?php _e('Visit Site','post-sync')?></a>
                </div>   
                <?php endif;?>
            </div>
        </div>
    </div>
</div>